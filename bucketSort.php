<?php 

function bucket_sort(&$elements) {
  $n = sizeof($elements);
  $buckets = array();
  // Initialize the buckets.
  for ($i = 0; $i < $n; $i++) {
    $buckets[$i] = array();
  }
  // Put each element into matched bucket.
  foreach ($elements as $el) {
    array_push($buckets[ceil($el/10)], $el);
  }
  // Sort elements in each bucket using insertion sort.
  $j = 0;
  for ($i = 0; $i < $n; $i++) {
    // sort only non-empty bucket
    if (!empty($buckets[$i])) {
      insertion_sort($buckets[$i]);
      // Move sorted elements in the bucket into original array.
      foreach ($buckets[$i] as $el) {
        $elements[$j++] = $el;
      }
    }
  }
}

$a = array(31, 40, 9, 1, 18, 3, 20, 14, 6, 10, 8, 13, 22, 23, 29, 30, 55);
var_dump($a);
bucket_sort($a); // Sort the elements
var_dump($a);

?>