<?php
$numbers = [21, 25, 100, 98, 89, 77];
 
for ($i = 0; $i < (count($numbers) - 1); $i++) {
 
    for ($j = 0; $j < (count($numbers) - 1); $j++) {
 
        if ($numbers[$j] > $numbers[$j + 1]) {
 
            $tmp = $numbers[$j];
 
            $numbers[$j] = $numbers[$j + 1];
 
            $numbers[$j + 1] = $tmp;
        }
    }
}
 
print_r($numbers);