<?php 
function primefactor($num) {
	$sqrt = sqrt($num);
	for ($i = 2; $i <= $sqrt; $i++) {
		if ($num % $i == 0) {
			return array_merge(primefactor($num/$i), array($i));
		}
	}
	return array($num);
}

$prime = primefactor(12590161); // this is a prime number
print_r($prime);

$composite = primefactor(65535); // this is a composite number
print_r($composite);
?>